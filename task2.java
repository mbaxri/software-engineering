package task2;

public class MainClass {

	public static void main(String args[]) {

		int[][] myNumbers = { { 5, 4, 4 }, { 4, 3, 4 }, { 3, 2, 4 }, { 2, 2, 2 }, { 3, 3, 4 }, { 1, 4, 4 },
				{ 4, 1, 1 } };

		int length = myNumbers.length;
		int[][] myIndex = new int[length][myNumbers[0].length];

		for (int i = 0; i < myIndex.length; i++) {
			for (int j = 0; j < myIndex[i].length; j++) {
				myIndex[i][j] = 0;
			}
		}

		int counter = 0;
		myIndex[0][0] = 1;

		for (int i = 0; i < myNumbers.length; i++) {
			for (int j = 0; j < myNumbers[i].length; j++) {

				if (j > 0) {

					if (myNumbers[i][j] == myNumbers[i][j - 1]) {

						myIndex[i][j] = myIndex[i][j - 1];

						break;
					}

				}

				if (i > 0) {

					if (myNumbers[i][j] == myNumbers[i - 1][j]) {

						myIndex[i][j] = myIndex[i - 1][j];

						break;
					}

				}

				if (myIndex[i][j] == 0) {

					counter++;
					myIndex[i][j] = counter;
				}

			}

		}
	
		System.out.println(counter);

	}

}

