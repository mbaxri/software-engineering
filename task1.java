package task1;

import java.util.ArrayList;
import java.util.Collections;

public class MainClass {

	public static void main(String args[]) {

		int n = 4; 
		int cars[] = { 6, 2, 12, 7 };
		int k = 3;

		System.out.print(carParkingRoof(cars, k));

	}

	public static int carParkingRoof(int cars[], int k) {
		ArrayList<Integer> slots = new ArrayList<>();

		for (int i : cars) {
			slots.add(i);
		}

		Collections.sort(slots);
		int minRoof = 10000;

		for (int a = 0; a < slots.size() - k + 1; a++) {

			if ((slots.get(a + k - 1) - slots.get(a) + 1) < minRoof) {

				minRoof = slots.get(a + k - 1) - slots.get(a) + 1;
			}

		}

		return minRoof;

	}

}

